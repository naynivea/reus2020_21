/**
 * 
 */
package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Button;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JList;
import java.awt.SystemColor;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Vista extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 574);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn1 = new JButton("+/-");
		btn1.setBounds(0, 474, 108, 53);
		contentPane.add(btn1);
		
		JButton btn2 = new JButton("0");
		btn2.setBounds(105, 474, 108, 53);
		contentPane.add(btn2);
		
		JButton btn3 = new JButton(",");
		btn3.setBounds(212, 474, 108, 53);
		contentPane.add(btn3);
		
		JButton btn4 = new JButton("=");
		btn4.setBounds(319, 474, 108, 53);
		contentPane.add(btn4);
		
		JButton btn8 = new JButton("+");
		btn8.setBounds(319, 422, 108, 53);
		contentPane.add(btn8);
		
		JButton btn7 = new JButton("3");
		btn7.setBounds(212, 422, 108, 53);
		contentPane.add(btn7);
		
		JButton btn6 = new JButton("2");
		btn6.setBounds(105, 422, 108, 53);
		contentPane.add(btn6);
		
		JButton btn5 = new JButton("1");
		btn5.setBounds(0, 422, 108, 53);
		contentPane.add(btn5);
		
		JButton btn12 = new JButton("_");
		btn12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn12.setBounds(319, 370, 108, 53);
		contentPane.add(btn12);
		
		JButton btn11 = new JButton("6");
		btn11.setBounds(212, 370, 108, 53);
		contentPane.add(btn11);
		
		JButton btn10 = new JButton("5");
		btn10.setBounds(105, 370, 108, 53);
		contentPane.add(btn10);
		
		JButton btn9 = new JButton("4");
		btn9.setBounds(0, 370, 108, 53);
		contentPane.add(btn9);
		
		JButton btn16 = new JButton("X");
		btn16.setBounds(319, 318, 108, 53);
		contentPane.add(btn16);
		
		JButton btn15 = new JButton("9");
		btn15.setBounds(212, 318, 108, 53);
		contentPane.add(btn15);
		
		JButton btn14 = new JButton("8");
		btn14.setBounds(105, 318, 108, 53);
		contentPane.add(btn14);
		
		JButton btn13 = new JButton("7");
		btn13.setBounds(0, 318, 108, 53);
		contentPane.add(btn13);
		
		JButton btn20 = new JButton("÷");
		btn20.setBounds(319, 265, 108, 53);
		contentPane.add(btn20);
		
		JButton btn19 = new JButton("√x");
		btn19.setBounds(212, 265, 108, 53);
		contentPane.add(btn19);
		
		JButton btn18 = new JButton("x²");
		btn18.setBounds(105, 265, 108, 53);
		contentPane.add(btn18);
		
		JButton btn17 = new JButton("1/x");
		btn17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn17.setBounds(0, 265, 108, 53);
		contentPane.add(btn17);
		
		JButton btn24 = new JButton("");
		btn24.setIcon(new ImageIcon("C:\\Users\\nayya\\OneDrive\\Imagens\\Screenshot_4.jpg"));
		btn24.setBounds(319, 212, 108, 53);
		contentPane.add(btn24);
		
		JButton btn23 = new JButton("C");
		btn23.setBounds(212, 212, 108, 53);
		contentPane.add(btn23);
		
		JButton btn22 = new JButton("CE");
		btn22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn22.setBounds(105, 212, 108, 53);
		contentPane.add(btn22);
		
		JButton btn21 = new JButton("%");
		btn21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn21.setBounds(0, 212, 108, 53);
		contentPane.add(btn21);
		
		JLabel resultado = new JLabel("0");
		resultado.setHorizontalAlignment(SwingConstants.RIGHT);
		resultado.setVerticalAlignment(SwingConstants.BOTTOM);
		resultado.setFont(new Font("Tahoma", Font.PLAIN, 50));
		resultado.setBounds(0, 121, 427, 91);
		contentPane.add(resultado);
		
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(463, 68, 148, 53);
		contentPane.add(lblNewLabel);
		
		JList list = new JList();
		list.setBackground(SystemColor.control);
		list.setBounds(430, 121, 264, 406);
		contentPane.add(list);
	}
}
