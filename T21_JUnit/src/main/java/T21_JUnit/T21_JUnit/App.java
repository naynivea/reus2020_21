package T21_JUnit.T21_JUnit;

import views.Vista;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
        Vista vista = new Vista();
        vista.setVisible(true);
    }
}
